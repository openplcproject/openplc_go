#!/usr/bin/env bash

# Ripped thank to following
# https://www.digitalocean.com/community/tutorials/how-to-build-go-executables-for-multiple-platforms-on-ubuntu-16-04
# https://github.com/mattn/go-sqlite3/issues/303
set -x

pwd
ls

package="gitlab.com/openplcproject/openplc_go/cmd/openplc_server"
if [[ -z "$package" ]]; then
  echo "usage: $0 <package-name>"
  exit 1
fi
package_split=(${package//\// })
package_name=${package_split[-1]}


HERE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
BUILD_DIR="$HERE_DIR/build/downloads"

mkdir -p $BUILD_DIR

INDEX_FILE="$HERE_DIR/build/index.md"

cd $HERE_DIR/server 
rice embed-go
cd $HERE_DIR

echo "# Downloads" > $INDEX_FILE
echo "" >> $INDEX_FILE

DATE=`date '+%Y-%m-%d %H:%M:%S'`
DATEN=`date '+%A, %d %B %Y at %H:%M'`H
echo "Built: $DATEN ($DATE)" >> $INDEX_FILE 
echo "" >> $INDEX_FILE 

# ALL Wanted
#platforms=( "linux/386" "linux/amd64" "linux/arm" "windows/amd64" "windows/386" "darwin/amd64" "darwin/386" "darwin/arm64" )

## Working
platforms=("linux/arm" "linux/386" "linux/amd64" "windows/amd64" "windows/386" "darwin/amd64")

#platforms=( "linux/arm" "linux/386" "linux/amd64" )

# echo "-----------------"
# which gcc-arm-linux-gnueabihf
# which arm-linux-gnueabihf-gcc
# which g++-arm-linux-gnueabihf
# which gcc-arm-linux-gnueabi

for platform in "${platforms[@]}"
do
    platform_split=(${platform//\// })
    GOOS=${platform_split[0]}
    GOARCH=${platform_split[1]}

    build_opts=""    
    output_name='openplc-server-'$GOOS'-'$GOARCH
    output_ext=""
    echo "---> $output_name"

    if [ $GOOS = "windows" ]; then
        ## Windows
        output_ext='.exe'
        #if [ $GOARCH = "amd64" ]; then
            #build_opts="CXX_FOR_TARGET=i686-w64-mingw32-g++ CC_FOR_TARGET=i686-w64-mingw32-gcc"
            
            #build_opts="CC=x86_64-w64-mingw32-gcc"
        #else 
            #build_opts="CC=i686-w64-mingw32-gcc"
        #fi

    #elif [ $GOOS = "linux" ]; then

        ## Linux ARM - sudo apt-get install libc6-armel-cross libc6-dev-armel-cross binutils-arm-linux-gnueabi libncurses5-dev
        #if [ $GOARCH = "arm" ]; then
            #Setting up g++-arm-linux-gnueabihf (4:6.3.0-4) ...
            #Setting up gcc-arm-linux-gnueabi (4:6.3.0-4) .
            #Setting up gcc-arm-linux-gnueabihf (4:6.3.0-4) ...
            #Setting up libgcc-6-dev-armel-cross (6.3.0-18cross1) ...
            #Setting up gcc-6-arm-linux-gnueabi (6.3.0-18cross1) ...
            #Setting up g++-arm-linux-gnueabihf (4:6.3.0-4) ...
            #Setting up gcc-arm-linux-gnueabi (4:6.3.0-4) ...
            ## ERROR "gcc-arm-linux-gnueabihf ---      gcc-arm-linux-gnueabi":
            #build_opts="CC=arm-linux-gnueabihf-gcc CXX=arm-linux-gnueabihf-g++ GOARM=7"
            #build_opts="CC=arm-linux-gnueabihf-gcc CXX=arm-linux-gnueabihf-g++ GOARM=7"
           # output_name+='7'
       
       # fi

    fi 
    #  
    env CGO_ENABLED=0 GOOS=$GOOS GOARCH=$GOARCH GOARM=5 $build_opts go build -ldflags="-s -w"  -o "$BUILD_DIR/$output_name-debug$output_ext" cmd/openplc_server/main.go
    upx -q -o "$BUILD_DIR/$output_name$output_ext" "$BUILD_DIR/$output_name-debug$output_ext"

    FD=`stat -c '%s' $BUILD_DIR/$output_name-debug$output_ext | numfmt --to=si`
    FS=`stat -c '%s' $BUILD_DIR/$output_name$output_ext | numfmt --to=si`
    
    if [ $? -ne 0 ]; then
        echo 'An error has occurred! Aborting the script execution...'
        exit 1
    fi
    echo "## $GOOS-$GOARCH" >> $INDEX_FILE
    echo " - <a href='/openplc_go/downloads/$output_name$output_ext'>$output_name$output_ext</a> ($FS)" >> $INDEX_FILE
    echo " - <a href='/openplc_go/downloads/$output_name-debug$output_ext'>$output_name-debug$output_ext</a> ($FD)" >> $INDEX_FILE
    
    echo "" >> $INDEX_FILE
    echo "" >> $INDEX_FILE
done
