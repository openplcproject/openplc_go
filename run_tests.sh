#!/usr/bin/env bash

HERE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## Checkiour openplc_tests
if [ -d ./openplc_tests ]   # for file "if [-f /home/rama/file]" 
then 
    cd ./openplc_tests
    git pull origin master
else
    git clone https://gitlab.com/openplcproject/openplc_tests.git 
fi


cd $HERE_DIR/tests 
go test -v



