package tests

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/openplcproject/openplc_go/server"
)

var WS_PATH = "../openplc_tests/workspace"

func init() {
	server.InitWorkSpace(WS_PATH)
}

func TestWorkSpacePaths(t *testing.T) {
	var pth string
	pth = server.WorkSpace.ProgramsDir()

	assert.DirExists(t, pth, "Programs Dir not exist")

	pth = server.WorkSpace.SlavesDir()
	assert.DirExists(t, pth)

	pth = server.WorkSpace.UsersDir()
	assert.DirExists(t, pth)

	// dirs := []string{"programs", "slaves", "users"}
	// for _, p := range dirs {
	// 	fmt.Println(p)
	// }

}
