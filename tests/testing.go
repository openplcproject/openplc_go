package tests

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
)

// Creates and calls a GET request, returning the Response
func CreateGetRequest(url string, fun http.HandlerFunc) (*httptest.ResponseRecorder, error) {

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(fun)
	handler.ServeHTTP(rr, req)

	return rr, nil
}

// Creates and calls a POST request, returning the Response
func CreatePostRequest(url string, fun http.HandlerFunc, data map[string]interface{}) (*httptest.ResponseRecorder, error) {

	jsonstr, err := json.Marshal(data)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonstr))
	if err != nil {
		return nil, err
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(fun)
	handler.ServeHTTP(rr, req)

	return rr, nil
}
