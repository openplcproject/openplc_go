package tests

import (
	"encoding/json"

	"net/http"

	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/openplcproject/openplc_go/server"
)

func TestProgramsFunc(t *testing.T) {

	recs, err := server.GetPrograms()
	assert.Nil(t, err)

	assert.NotNil(t, recs)
	assert.Equal(t, len(recs), 1)

}

func TestProgramsAjax(t *testing.T) {

	rr, err := CreateGetRequest("/ajax/login", server.AxPrograms)
	if err != nil {
		t.Fatal(err)
	}
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var data map[string]interface{}
	if err := json.NewDecoder(rr.Body).Decode(&data); err != nil {
		t.Errorf("handler not return json: got `%v`",
			rr.Body.String())
	}
	errj := data["error"]
	assert.Nil(t, errj)

	//fmt.Println(errj)
	programs, ok := data["programs"].([]interface{})
	//set1, ok := settings["modbus_port"]
	assert.Equal(t, ok, true)
	assert.NotNil(t, programs)

	//fmt.Println(programs, ok )

}
