package tests

import (
	"encoding/json"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/openplcproject/openplc_go/server"
)

func TestLoginOkAjax(t *testing.T) {

	//
	return
	sdata := map[string]interface{}{
		"login":    "openplc",
		"password": "openplc",
	}

	rr, err := CreatePostRequest("/ajax/login", server.AxLogin, sdata)
	if err != nil {
		t.Fatal(err)
	}

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var data map[string]interface{}
	if err := json.NewDecoder(rr.Body).Decode(&data); err != nil {
		t.Errorf("handler not return json: got `%v`",
			rr.Body.String())
	}
	errj := data["error"]
	assert.Nil(t, errj)

}
func TestLoginVarsAjax(t *testing.T) {

	sdata := map[string]interface{}{
		"logiin":    "openplc",
		"passsword": "openplc",
	}

	rr, err := CreatePostRequest("/ajax/login", server.AxLogin, sdata)
	if err != nil {
		t.Fatal(err)
	}

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var data map[string]interface{}
	if err := json.NewDecoder(rr.Body).Decode(&data); err != nil {
		t.Errorf("handler not return json: got `%v`",
			rr.Body.String())
	}
	errj := data["error"]
	assert.NotNil(t, errj)
	assert.Equal(t, errj.(string), server.ErrCredentialsMissing.Error())

}

func TestLoginAuthFailAjax(t *testing.T) {

	sdata := map[string]interface{}{
		"login":    "openplc",
		"password": "what",
	}

	rr, err := CreatePostRequest("/ajax/login", server.AxLogin, sdata)
	if err != nil {
		t.Fatal(err)
	}

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var data map[string]interface{}
	if err := json.NewDecoder(rr.Body).Decode(&data); err != nil {
		t.Errorf("handler not return json: got `%v`",
			rr.Body.String())
	}
	errj := data["error"]
	assert.NotNil(t, errj)
	assert.Equal(t, errj.(string), server.ErrAuthFail.Error())
}
