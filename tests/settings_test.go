package tests

import (
	"encoding/json"
	//"fmt"
	"net/http"

	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/openplcproject/openplc_go/server"
)

func TestSettingsGet(t *testing.T) {

	//foo, err := LoadSettings()

}

func TestSettingsAjax(t *testing.T) {

	rr, err := CreateGetRequest("/ajax/login", server.AxSettings)
	if err != nil {
		t.Fatal(err)
	}
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var data map[string]interface{}
	if err := json.NewDecoder(rr.Body).Decode(&data); err != nil {
		t.Errorf("handler not return json: got `%v`",
			rr.Body.String())
	}
	errj := data["error"]
	assert.Nil(t, errj)
	settings := data["settings"].(map[string]interface{})
	_, ok1 := settings["modbus_port"]
	assert.Equal(t, ok1, true)
	//assert.Equal(t, uint(504), set1.(uint))

	_, ok2 := settings["modbus_portssss"]
	assert.Equal(t, ok2, false)
	//assert.Equal(t, set2, nil)

	// Check the response body is what we expect.
	// expected := `[{"id":1,"first_name":"Krish","last_name":"Bhanushali","email_address":"krishsb@g.com","phone_number":"0987654321"},{"id":2,"first_name":"xyz","last_name":"pqr","email_address":"xyz@pqr.com","phone_number":"1234567890"},{"id":6,"first_name":"FirstNameSample","last_name":"LastNameSample","email_address":"lr@gmail.com","phone_number":"1111111111"}]`
	// if rr.Body.String() != expected {
	// 	t.Errorf("handler returned unexpected body: got %v want %v",
	// 		rr.Body.String(), expected)
	// }

}
