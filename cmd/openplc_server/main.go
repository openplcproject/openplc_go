package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"gitlab.com/openplcproject/openplc_go/server"
)

func main() {

	var fWorkspace = flag.String("w", "", "Path to workspace directory")
	var fOpenPlc = flag.String("o", "", "Path to OpenPLC_v3/ directory")
	var fAddress = flag.String("a", "127.0.0.1:8080", "Server address")

	var fDebug = flag.Bool("d", false, "Debug mode")
	var fControlPanel = flag.String("c", "./control-panel", "Path to control-panel build")

	flag.Parse()

	if *fWorkspace == "" {
		fmt.Println("ERROR: Need -w workspace dir option")
		return
	}

	clean_ws, err := filepath.Abs(*fWorkspace)
	if err != nil {
		fmt.Println("ERROR: `" + err.Error() + "")
		return
	}
	// check workspace exists
	sta, err := os.Stat(clean_ws)
	if os.IsNotExist(err) {
		fmt.Println("ERROR: Directory `" + clean_ws + "` does not exist")
		return
	}
	// check workspace is a directory
	if sta.Mode().IsRegular() {
		fmt.Println("ERROR: `" + clean_ws + "`  exists as a file!")
		return
	}

	// If its empty then init
	empty, err := isEmpty(clean_ws)
	if empty {
		server.WorkSpace.Create(clean_ws)
		fmt.Println("====== Important =======\nEmpty Workspace detected\nCreated default user:")
		fmt.Println("Login: openplc")
		fmt.Println("Password: openplc")
		fmt.Println("========================\n")
	}

	// initialise
	errr := server.InitSetup(clean_ws, *fOpenPlc, *fDebug, *fControlPanel)
	if errr != nil {
		fmt.Printf("Init error: %v\n", errr)
		os.Exit(1)
	}

	fmt.Println("#> openplc-server")
	fmt.Println("Workspace: " + clean_ws)
	fmt.Println("Listening: " + *fAddress)

	//var httperr = http.ListenAndServe(*fAddress, router)
	var httperr = server.ListenAndServe(*fAddress)
	fmt.Printf("Server error: %v\n", httperr)
}

func isEmpty(path string) (bool, error) {
	f, err := os.Open(path)
	if err != nil {
		return false, err
	}
	defer f.Close()

	_, err = f.Readdirnames(1) // Or f.Readdir(1)
	if err == io.EOF {
		return true, nil
	}
	return false, err // Either not empty or error, suits both cases
}
