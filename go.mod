module gitlab.com/openplcproject/openplc_go

require (
	github.com/creack/goselect v0.0.0-20180501195510-58854f77ee8d // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.30.0+incompatible
	github.com/gorilla/mux v1.7.1
	github.com/gorilla/sessions v1.2.0
	github.com/gorilla/websocket v1.4.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/teris-io/shortid v0.0.0-20171029131806-771a37caa5cf
	github.com/urfave/negroni v1.0.0
	go.bug.st/serial.v1 v0.0.0-20180827123349-5f7892a7bb45
	golang.org/x/crypto v0.0.0-20190422183909-d864b10871cd
	golang.org/x/sys v0.0.0-20190422165155-953cdadca894 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
