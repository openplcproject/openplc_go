package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 1 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 2 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = 1 * time.Second //(pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Hub maintains the set of active clients and broadcasts messages to the clients.
type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	//broadcast chan []byte
	broadcast chan WsSendMess

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
}

var WsHub *Hub

type WsSendMess struct {
	Ns    string      ` json:"ns" `
	MType string      ` json:"mtype" `
	Data  interface{} ` json:"data" `
	Ts    int64       ` json:"ts" `
}
type WsRecvMess struct {
	Ns   string      ` json:"ns" `
	Cmd  string      ` json:"cmd" `
	Data interface{} ` json:"data" `
	//Ts   string      ` json:"ts" `
}

func AxWsDebug(w http.ResponseWriter, r *http.Request) {

	payload := makePayload()

	// Check its not already compoiling
	SendWsMessage("foo", "bar", payload)
	SendJSONResponse(w, payload)

}

func SendWsMessage(ns string, mtype string, data interface{}) {
	// pay := make(map[string]interface{})
	// pay["ns"] = ns
	// pay["mtype"] = mtype
	// pay["data"] = data
	// pay["ts"] = time.Now().Unix()
	//fmt.Println("> ", ns, data)
	pay := WsSendMess{Ns: ns, MType: mtype, Data: data, Ts: time.Now().Unix()}
	//json_str, _ := json.MarshalIndent(pay, "", "  ")
	//json_str, _ := json.Marshal(pay)
	//WsHub.broadcast <- json_str
	WsHub.broadcast <- pay
}

func NewWsHub() *Hub {
	return &Hub{
		//broadcast:  make(chan []byte),
		broadcast:  make(chan WsSendMess),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}
}

func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.broadcast:
			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	hub *Hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	//send chan []byte
	send chan WsSendMess
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, raw_bytes, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}
		raw_bytes = bytes.TrimSpace(bytes.Replace(raw_bytes, newline, space, -1))

		//fmt.Println("Got WS Mess", string(raw_bytes))

		var mess WsRecvMess
		err = json.Unmarshal(raw_bytes, &mess)
		if err != nil {
			// TODO add error log
			fmt.Println(err)
		} else {

			if mess.Ns == "" {
				fmt.Println("NO ns")
				return
			}
			if mess.Cmd == "" {
				fmt.Println("NO cmd")
				return
			}
			if mess.Ns == "runtime" {
				switch mess.Cmd {
				case "start":
					go ExecuteStartRuntime()

				case "stop":
					StopRuntime()
				}
			}

		}

		//c.hub.broadcast <- raw_bytes

	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			pay := make([]WsSendMess, 0)
			//fmt.Println("len=", len(c.send), message)
			//w.Write(message)
			pay = append(pay, message)

			// Add queued chat messages to the current websocket message.
			//fmt.Println("len=", len(c.send))
			n := len(c.send)
			for i := 0; i < n; i++ {
				//w.Write(newline)
				//w.Write(<-c.send)
				//w.Close()
				pay = append(pay, <-c.send)
			}
			jsonBytes, errj := json.Marshal(pay)
			if errj != nil {
				fmt.Println("josn error")
			}
			w.Write(jsonBytes)
			w.Write(newline)
			//fmt.Println(string(jsonBytes))
			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// ServeWs handles websocket requests from the peer.
func ServeWs(hub *Hub, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	client := &Client{hub: hub, conn: conn, send: make(chan WsSendMess, 256)}
	client.hub.register <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()

}
