package server

import (
	"net/http"
	"os"
)

// Global Settings
var Settings *SettingsOb

func init() {
	Settings = new(SettingsOb)
}

func LoadSettings() {

	// check settings exists or init blank
	_, err := os.Stat(Settings.FileName())
	if os.IsNotExist(err) {
		Settings.SetDefaults()
		Settings.Save()
		return
	}
	Settings.Load()
}

type SettingsOb struct {
	ModbusEnabled bool ` json:"modbus_enabled" `
	ModbusPort    uint ` json:"modbus_port" `

	Dnp3Enabled bool ` json:"dnp3_enabled" `
	Dnp3Port    uint ` json:"dnp3_port" `

	StartRunMode bool ` json:"start_run_mode" `

	SlavePolling int ` json:"slave_polling" `
	SlaveTimeout int ` json:"slave_timeout" `
}

func (se *SettingsOb) SetDefaults() {
	se.ModbusEnabled = true
	se.ModbusPort = 503

	se.Dnp3Enabled = false
	se.Dnp3Port = 20000

	se.StartRunMode = true

	se.SlavePolling = 100
	se.SlaveTimeout = 1000
}

func (se *SettingsOb) Save() error {
	return WriteJSON(se.FileName(), se)
}

func (se *SettingsOb) Load() error {
	return ReadJSON(se.FileName(), se)
}

func (se *SettingsOb) FileName() string {
	return WorkSpace.RootDir("/settings.json")
}

// Settings request - /ajax/settings
func AxSettings(resp http.ResponseWriter, req *http.Request) {

	var err error
	payload := makePayload()

	if req.Method == "POST" {

		payload["saved"] = false
		var sett SettingsOb
		err = LoadObjectFromRequestBody(&sett, req)
		if err != nil {
			payload["error"] = err.Error()
		} else {
			sett.Save()
			LoadSettings()
			payload["saved"] = true
		}
	}

	payload["settings"] = Settings

	SendJSONResponse(resp, payload)
}
