package server

import (
	"fmt"
	"net/http"
	"time"

	//"io"
	//"fmt"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

var router *mux.Router

func init() {

	router = mux.NewRouter().StrictSlash(false)

	//= /ajax/* routes
	ajaxRouter := router.PathPrefix("/ajax").Subrouter()

	ajaxRouter.HandleFunc("/debug/ws", AxWsDebug).Methods("GET")

	ajaxRouter.HandleFunc("/login", AxLogin).Methods("GET", "POST")

	ajaxRouter.HandleFunc("/ping", AxPing).Methods("GET", "POST")
	ajaxRouter.HandleFunc("/info", AxInfo).Methods("GET", "POST")
	ajaxRouter.HandleFunc("/settings", AxSettings).Methods("GET", "POST")

	ajaxRouter.HandleFunc("/runtime/start", AxRuntimeStart).Methods("GET", "POST")
	ajaxRouter.HandleFunc("/runtime/stop", AxRuntimeStop).Methods("GET", "POST")

	ajaxRouter.HandleFunc("/slave-devices", AxSlaves).Methods("GET")
	ajaxRouter.HandleFunc("/slave-devices/{dev_id}", AxSlave).Methods("GET", "POST", "DELETE")
	ajaxRouter.HandleFunc("/modbus-config", AxModbusConfig).Methods("GET")

	ajaxRouter.HandleFunc("/serial-ports", AxSerialPorts).Methods("GET")

	ajaxRouter.HandleFunc("/programs", AxPrograms).Methods("GET")
	ajaxRouter.HandleFunc("/programs/{prog_id}", AxProgram).Methods("GET", "POST", "DELETE")
	ajaxRouter.HandleFunc("/compile-program", AxCompileProgram).Methods("GET", "POST")

	ajaxRouter.HandleFunc("/users", AxUsers).Methods("GET")
	ajaxRouter.HandleFunc("/users/{user_id}", AxUser).Methods("GET", "POST", "DELETE")

	ajaxRouter.HandleFunc("/{path:.*}", Ax404).Methods("GET")

	//= html pages
	// router.HandleFunc("/login", P_Login).Methods("GET", "POST")
	// router.HandleFunc("/slaves", P_Slaves).Methods("GET", "POST")
	router.HandleFunc("/", P_Home).Methods("GET")

	//= static files
	//staticFileServer := http.StripPrefix("/static/", http.FileServer(StaticBox.HTTPBox()))
	//router.Handle("/static/{path:.*}", staticFileServer)

	//= websocket
	router.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		ServeWs(WsHub, w, r)
	})
	ticker := time.NewTicker(10 * time.Second)
	go func() {
		for t := range ticker.C {
			//fmt.Println("Tick at", t)
			//if t {
			fmt.Println(t)
			//}
			//SendCommand("exec_time()")
			//SendCommand("runtime_logs()")
			SendWsMessage("ping", "ping", "")
		}
	}()
}

// Initialise and Setup server
func InitSetup(workspace_path string, openplc_path string, debug bool, control_panel string) error {

	OpenPLCDir = openplc_path

	InitWorkSpace(workspace_path)
	LoadConfig()
	InitSession()
	LoadSettings()

	//= vue control-panel
	fileServer := http.FileServer(http.Dir(control_panel))
	router.Handle("/cp/{path:.*}", http.StripPrefix("/cp/", fileServer))

	WsHub = NewWsHub()
	go WsHub.Run()

	return nil
}

func GetRoutesInfo() map[string][]string {
	lst := make(map[string][]string, 0)
	router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {

		tpl, err1 := route.GetPathTemplate()
		if err1 == nil {
		}
		met, err2 := route.GetMethods()
		if err2 == nil {
		}
		lst[tpl] = met
		return nil
	})
	return lst
}

func ListenAndServe(address string) error {

	neg := negroni.New()

	//neg.Use(negroni.NewRecovery())
	neg.Use(negroni.HandlerFunc(authMiddleware))

	neg.UseHandler(router)

	return http.ListenAndServe(address, neg)
}
