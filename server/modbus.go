package server

import (
	"net/http"
	"strconv"
)

func StartModbus(port_num int) (string, error) {
	return SendCommand("start_modbus(" + string(port_num) + ")")
}

func StopModbus(port_num int) (string, error) {
	return SendCommand("stop_modbus(" + string(port_num) + ")")
}

func AxModbusConfig(resp http.ResponseWriter, req *http.Request) {

	data := makePayload()

	var err error
	data["mbconfig"], err = GetMBConfig()
	if err != nil {
		SendJSONError(resp, err)
	}

	SendJSONResponse(resp, data)
}

func mb_int_line(idx int64, name string, val int64) string {
	return "device" + strconv.FormatInt(idx, 10) + "." + name + " = " + strconv.FormatInt(val, 10) + "\n"
}
func mb_str_line(idx int64, name string, val string) string {
	return "device" + strconv.FormatInt(idx, 10) + "." + name + " = " + val + "\n"
}
func WriteModbusConfig() error {

	mbconfig, err := GetMBConfig()
	if err != nil {
		return err
	}
	return WriteFile(OpenPLCDir+"/build/mbconfig.cfg", mbconfig)
}

// Writes out the `mbconfig.cfg` file
func GetMBConfig() (string, error) {

	slaves, err := GetSlaves()
	if err != nil {
		return "", err
	}
	var mbconfig string
	for idx, sl := range slaves {
		mbconfig += getSlaveMBConfig(int64(idx), sl)
	}
	return mbconfig, nil

}

func getSlaveMBConfig(idx int64, sl Slave) string {

	//idx := strconv.FormatInt(int64(device_idx), 10)

	var mbconfig string

	mbconfig += mb_int_line(idx, "IP_Port", sl.IPPort)

	mbconfig += mb_int_line(idx, "RTU_Baud_Rate", sl.BaudRate)
	mbconfig += mb_str_line(idx, "RTU_Parity", sl.Parity)
	mbconfig += mb_int_line(idx, "RTU_Data_Bits", sl.DataBits)
	mbconfig += mb_int_line(idx, "RTU_Stop_Bits", sl.StopBits)

	mbconfig += mb_int_line(idx, "Discrete_Inputs_Start", sl.DiStart)
	mbconfig += mb_int_line(idx, "Discrete_Inputs_Size", sl.DiSize)

	mbconfig += mb_int_line(idx, "Coils_Start", sl.CoilStart)
	mbconfig += mb_int_line(idx, "Coils_Size", sl.CoilSize)

	mbconfig += mb_int_line(idx, "Input_Registers_Start", sl.IrStart)
	mbconfig += mb_int_line(idx, "Input_Registers_Size", sl.IrSize)

	mbconfig += mb_int_line(idx, "Holding_Registers_Read_Start", sl.HrReadStart)
	mbconfig += mb_int_line(idx, "Holding_Registers_Read_Size", sl.HrReadSize)

	mbconfig += mb_int_line(idx, "Holding_Registers_Start", sl.HrWriteStart)
	mbconfig += mb_int_line(idx, "Holding_Registers_Size", sl.HrWriteSize)

	return mbconfig
}
