package server

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

type Program struct {
	ProgID      string ` json:"prog_id" `
	Name        string ` json:"name" `
	Description string ` json:"description" `
	FileName    string ` json:"file_name" `
	DateUpload  string ` json:"date_upload" `

	Active      bool   ` json:"active" `
	DateCreated string ` json:"date_created" `
	DateUpdated string ` json:"date_updated" `
	DateDeleted string ` json:"date_deleted" `
}

func (prog Program) FilePath() string {
	return WorkSpace.ProgramsDir(prog.ProgID)
}

func (prog Program) Save() error {
	prog.DateUpdated = NowStr()
	return WriteJSON(prog.FilePath(), prog)
}

func GetPrograms() ([]Program, error) {

	programs := make([]Program, 0)

	files, err := ListFiles(WorkSpace.ProgramsDir())
	if err != nil {
		return programs, err
	}

	for _, prid := range files {

		pro, err := GetProgram(prid)
		if err != nil {
			fmt.Println("TODO ERRR EAD PRogram", err)
			//return programs, err
		} else {
			programs = append(programs, pro)
		}
	}
	return programs, nil
}

func GetProgram(prog_id string) (Program, error) {
	var pro Program
	pro.ProgID = prog_id
	//fmt.Println(pro.FilePath())
	err := ReadJSON(pro.FilePath(), &pro)
	return pro, err
}

// Programs list - /ajax/programs
func AxPrograms(resp http.ResponseWriter, req *http.Request) {

	payload := makePayload()

	payload["programs"], payload["error"] = GetPrograms()

	SendJSONResponse(resp, payload)
}

// Program CRUD - /ajax/program/{prog_id}
func AxProgram(resp http.ResponseWriter, req *http.Request) {

	var err error
	vars := mux.Vars(req)
	payload := makePayload()

	// get dev_id and validate
	prog_id, ok := vars["prog_id"]
	if ok && len(prog_id) < 3 {
		payload["error"] = "Missing `prog_id`"
		payload["program"] = nil
		SendJSONResponse(resp, payload)
		return
	}

	if req.Method == "DELETE" {
		//payload["deleted"], payload["error"] = DeleteProgram(prog_id)
		SendJSONResponse(resp, payload)
		return
	}

	if req.Method == "POST" {

		payload["saved"] = false
		var proUp Program
		var proCurr Program

		err = LoadObjectFromRequestBody(&proUp, req)
		if err != nil {
			payload["error"] = err.Error()
		}

		if prog_id == "new" {
			proCurr = Program{}
			proCurr.ProgID = GenUID()
			proCurr.DateCreated = NowStr()
			prog_id = proCurr.ProgID

		} else {
			proCurr, err = GetProgram(prog_id)
		}

		proCurr.Active = proUp.Active
		proCurr.Name = proUp.Name
		proCurr.Description = proUp.Description
		proCurr.DateUpdated = NowStr()

		err = proCurr.Save()
		if err != nil {
			SendJSONError(resp, err)
			return
		}

		payload["saved"] = true

	}

	if prog_id == "new" {
		payload["program"] = Program{ProgID: prog_id}
	} else {
		payload["program"], payload["error"] = GetProgram(prog_id)
	}

	SendJSONResponse(resp, payload)
}

// Compiles program  /ajax/compile-program
func AxCompileProgram(resp http.ResponseWriter, req *http.Request) {

	payload := makePayload()

	// Check its not already compoiling
	go ExecuteCompileProgram()

	SendJSONResponse(resp, payload)
}
