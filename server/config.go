package server

import (
	"os"
)

// The main `config` reader/struct
type config struct {
	Debug       bool   `json:"debug"`
	HTTPAddress string `json:"http_address"`

	HashKey  string `json:"hash_key"`
	BlockKey string `json:"block_key"`
}

func (con *config) Save() error {
	return WriteJSON(con.FileName(), con)
}

func (con *config) Load() error {
	return ReadJSON(con.FileName(), con)
}

func (con *config) FileName() string {
	return WorkSpace.RootDir("/config.json")
}

func (con *config) SetDefaults() {

	con.HashKey = GenUID() + GenUID() + GenUID()
	con.BlockKey = (GenUID() + GenUID() + GenUID() + GenUID() + GenUID())[0:32]

	con.HTTPAddress = "127.0.0.1:8080"
}

// Global Settings
var Config *config

func init() {
	Config = new(config)
}

func LoadConfig() {

	// check settings exists or init blank
	_, err := os.Stat(Config.FileName())
	if os.IsNotExist(err) {
		Config.SetDefaults()
		Config.Save()
		return
	}
	Config.Load()
}
