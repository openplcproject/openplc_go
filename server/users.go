package server

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

type User struct {
	UserID   string ` json:"user_id" `
	Login    string ` json:"login" `
	PassHash string ` json:"password_hash" `

	Name  string ` json:"name" `
	Email string ` json:"email" `

	Active      bool   ` json:"active" `
	DateCreated string ` json:"date_created" `
	DateUpdated string ` json:"date_updated" `
	DateDeleted string ` json:"date_deleted" `
}

func (user User) FilePath() string {
	return WorkSpace.UsersDir(user.UserID)
}

func (user User) Save() error {
	user.DateUpdated = NowStr()
	return WriteJSON(user.FilePath(), user)
}

// Create new user object
func NewUser(login, pass string) User {
	user := User{Login: login, Active: true}
	user.UserID = GenUID()
	user.PassHash, _ = HashPassword(pass)
	user.DateCreated = NowStr()
	return user
}

// Returns all users from workspace
func GetUsers() ([]User, error) {

	users := make([]User, 0)

	files, err := ListFiles(WorkSpace.UsersDir())
	if err != nil {
		return users, err
	}

	for _, user_id := range files {
		user, err := GetUser(user_id)
		if err != nil {
			fmt.Println("TODO", err)
			//return users, err
		} else {
			users = append(users, user)
		}
	}
	return users, nil
}

// Users list - /ajax/users
func AxUsers(resp http.ResponseWriter, req *http.Request) {

	payload := makePayload()
	payload["users"], payload["error"] = GetUsers()

	SendJSONResponse(resp, payload)
}

var ErrUserNotFound = errors.New("User Not Found")

// Returns a single user from workspace by ID
func GetUser(user_id string) (User, error) {
	var user User
	user.UserID = user_id
	err := ReadJSON(user.FilePath(), &user)
	return user, err
}

// Returns a single user from workspace by ID
func GetUserByLogin(login string) (*User, error) {

	users, err := GetUsers()
	if err != nil {
		return nil, err
	}
	for _, user := range users {
		if user.Login == login {
			return &user, nil
		}
	}
	return nil, ErrUserNotFound
}

// User - /ajax/users/{user_id}
func AxUser(resp http.ResponseWriter, req *http.Request) {

	payload := makePayload()

	params := mux.Vars(req)
	user_id := params["user_id"]
	// TODO vaidate ID

	if req.Method == http.MethodPost {

		var userUp map[string]interface{}
		var userCurr User
		err := LoadObjectFromRequestBody(&userUp, req)
		if err != nil {
			SendJSONError(resp, err)
			return
		}

		if user_id == "new" {
			userCurr = User{}
			userCurr.DateCreated = NowStr()
			userCurr.UserID = GenUID()
			user_id = userCurr.UserID

		} else {
			userCurr, err = GetUser(user_id)
		}

		passs, ok := userUp["password"]
		if ok && len(passs.(string)) > 4 {
			userCurr.PassHash, err = HashPassword(passs.(string))
		}

		// This is a pain, but ain't figured out a better way
		userCurr.Active = userUp["active"].(bool)
		userCurr.Login = userUp["login"].(string)
		userCurr.Name = userUp["name"].(string)
		userCurr.Email = userUp["email"].(string)
		userCurr.DateUpdated = NowStr()

		userCurr.Save()

	}

	if user_id == "new" {
		payload["user"] = User{Active: true, UserID: user_id}
	} else {
		payload["user"], payload["error"] = GetUser(user_id)
	}

	

	SendJSONResponse(resp, payload)
}
