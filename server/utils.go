package server

import (
	"strconv"
	"time"

	"github.com/teris-io/shortid"
)

const (
	DATE_FORMAT = "2006-01-02 15:04:05"
)

func NowStr() string {
	t := time.Now()
	return t.Format(DATE_FORMAT)
}

func DateFromEpoch(date string) (string, error) {
	i, err := strconv.ParseInt(date, 10, 64)
	if err != nil {
		return "", err
	}
	t := time.Unix(i, 0)
	return t.Format(DATE_FORMAT), nil
}

func GenUID() string {
	s, _ := shortid.Generate()
	return s
}
