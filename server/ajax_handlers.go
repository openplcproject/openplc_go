package server

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func makePayload() map[string]interface{} {
	pay := make(map[string]interface{})
	pay["error"] = nil
	return pay
}

func LoadObjectFromRequestBody(object interface{}, req *http.Request) error {

	b, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		return err
	}
	//fmt.Println(string(b))

	err = json.Unmarshal(b, object)
	if err != nil {
		return err
	}
	return nil
}

// Writes out the response payload in json
func SendJSONResponse(resp http.ResponseWriter, payload interface{}) {

	resp.Header().Set("Content-Type", "application/json")
	indent := ""
	if true { //conf.JSONPretty {
		indent = "  "
	}
	jsonstr, _ := json.MarshalIndent(payload, "", indent)
	fmt.Fprint(resp, string(jsonstr))
}

// Writes out the response error in json
func SendJSONError(resp http.ResponseWriter, err error) {

	resp.Header().Set("Content-Type", "application/json")
	indent := ""
	if true { //conf.JSONPretty {
		indent = "  "
	}
	payload := map[string]string{"error": err.Error()}
	jsonstr, _ := json.MarshalIndent(payload, "", indent)
	fmt.Fprint(resp, string(jsonstr))

}

// Simple Ping /ajax/ping
func AxPing(resp http.ResponseWriter, req *http.Request) {

	data := makePayload()
	data["ping"] = "pong"

	SendJSONResponse(resp, data)
}

// 404 /ajax/ping
func Ax404(resp http.ResponseWriter, req *http.Request) {

	data := makePayload()
	data["error"] = "404: Not Found"

	resp.WriteHeader(http.StatusNotFound)
	SendJSONResponse(resp, data)
}

// Ajax Info /ajax/info
func AxInfo(resp http.ResponseWriter, req *http.Request) {

	data := makePayload()

	data["routes"] = GetRoutesInfo()
	data["workspace"] = WorkSpace.baseDir

	SendJSONResponse(resp, data)
}
