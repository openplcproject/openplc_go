package server

import (
	"io/ioutil"
	"os"
	"strings"

	"encoding/json"
)

const (
	DEFAULT_FILE_MODE = 0755
)

// Global instance
var WorkSpace *WorkSpaceStruct

func init() {
	WorkSpace = new(WorkSpaceStruct)
}

// Setup Workspace
func InitWorkSpace(pth string) error {
	return WorkSpace.SetBaseDir(pth)
}

type WorkSpaceStruct struct {
	baseDir string
}

func (ws *WorkSpaceStruct) getFilename(filex []string) string {
	if len(filex) == 0 {
		return ""
	}
	return "/" + filex[0] + ".json"
}

func (ws *WorkSpaceStruct) RootDir(extra ...string) string {
	if len(extra) == 0 {
		return ws.baseDir
	}
	return ws.baseDir + extra[0]
}

func (ws *WorkSpaceStruct) ProgramsDir(prog_id ...string) string {
	return ws.baseDir + "/programs" + ws.getFilename(prog_id)
}

func (ws *WorkSpaceStruct) SlavesDir(dev_id ...string) string {
	return ws.baseDir + "/slaves" + ws.getFilename(dev_id)
}

func (ws *WorkSpaceStruct) UsersDir(user_id ...string) string {
	return ws.baseDir + "/users" + ws.getFilename(user_id)
}

func (ws *WorkSpaceStruct) UploadsDir() string {
	return ws.baseDir + "/uploads"
}

func (ws *WorkSpaceStruct) SessionsDir() string {
	return ws.baseDir + "/__session__"
}

func (ws *WorkSpaceStruct) SetBaseDir(pth string) error {
	ws.baseDir = pth
	//LoadSettings()
	return nil
}

func (ws *WorkSpaceStruct) Create(path string) error {

	ws.SetBaseDir(path)

	// create dirs if not exist
	_ = os.Mkdir(ws.ProgramsDir(), DEFAULT_FILE_MODE)
	_ = os.Mkdir(ws.SlavesDir(), DEFAULT_FILE_MODE)
	_ = os.Mkdir(ws.UsersDir(), DEFAULT_FILE_MODE)

	_ = os.Mkdir(ws.UploadsDir(), DEFAULT_FILE_MODE)

	_ = os.Mkdir(ws.SessionsDir(), DEFAULT_FILE_MODE)

	// Create and load settings
	LoadConfig()
	LoadSettings()

	// Create user
	user := NewUser("openplc", "openplc")
	user.Save()

	return nil
}

func ListFiles(path string) ([]string, error) {
	var files []string
	rfiles, err := ioutil.ReadDir(path)
	for _, file := range rfiles {

		if file.IsDir() {
			continue
		}
		parts := strings.Split(file.Name(), ".")
		files = append(files, parts[0])

	}
	return files, err
}

func WriteFile(path, contents string) error {
	return ioutil.WriteFile(path, []byte(contents), DEFAULT_FILE_MODE)
}

func WriteJSON(filepath string, object interface{}) error {

	jstr, err := json.MarshalIndent(object, "", "  ")
	if err != nil {
		return err
	}
	return WriteFile(filepath, string(jstr))
}
func ReadJSON(filepath string, object interface{}) error {

	file, errf := ioutil.ReadFile(filepath)
	if errf != nil {
		return errf
	}

	err := json.Unmarshal(file, object)
	if err != nil {
		return err
	}
	return nil
}
