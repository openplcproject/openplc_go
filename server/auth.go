package server

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gorilla/sessions"
	"golang.org/x/crypto/bcrypt"
)

const (
	SESSION_NAME = "openplc-session"
)

var store *sessions.CookieStore

func InitSession() {

	store = sessions.NewCookieStore(
		[]byte(Config.HashKey),
		[]byte(Config.BlockKey),
	)

	store.Options = &sessions.Options{
		MaxAge:   60 * 24,
		HttpOnly: true,
	}

}

// Authentication middleware
func authMiddleware(resp http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	session, _ := store.Get(req, SESSION_NAME)

	// Some endpoints don't need auth
	for _, u := range []string{"/", "/ajax/login", "/ajax/info", "/ajax/ping", "/ws"} {
		if u == req.URL.Path {
			next(resp, req)
			return
		}
	}

	// Check user has session
	login := session.Values["login"]
	if login == nil {
		resp.WriteHeader(http.StatusForbidden)
		SendJSONError(resp, ErrNotAuth)
		return
	}

	next(resp, req)
}

var ErrCredentialsMissing = errors.New("Login or Password missing")
var ErrAuthFail = errors.New("Authentication failed")
var ErrNotAuth = errors.New("Not Authenticated")

func setSession(resp http.ResponseWriter, req *http.Request, login string, user_id string) {
	session, _ := store.Get(req, SESSION_NAME)

	session.Values["login"] = login
	session.Values["user_id"] = user_id
	session.Values["auth"] = true
	session.Save(req, resp)

}

// Creates Password
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// Check password
func CheckPasswordHash(hash, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// Ajax login - /ajax/login
func AxLogin(resp http.ResponseWriter, req *http.Request) {

	payload := makePayload()

	if req.Method == http.MethodGet {
		_, ok := req.URL.Query()["debug"]
		if ok {
			setSession(resp, req, "openplc", "1234565")
			payload["logged_in"] = true
			SendJSONResponse(resp, payload)
			return
		}
	}

	if req.Method == http.MethodPost {

		payload["logged_in"] = false

		// decode json
		var pdata map[string]interface{}
		err := json.NewDecoder(req.Body).Decode(&pdata)
		if err != nil {
			SendJSONError(resp, err)
			return

		}

		// vars
		login := pdata["login"]
		password := pdata["password"]
		if login == nil || password == nil {
			SendJSONError(resp, ErrCredentialsMissing)
			return
		}

		// get user
		user, erru := GetUserByLogin(login.(string))
		if erru != nil {
			SendJSONError(resp, erru)
			return
		}

		// check pass
		ok := CheckPasswordHash(user.PassHash, password.(string))
		if !ok {
			SendJSONError(resp, ErrAuthFail)
			return
		}

		// set session
		setSession(resp, req, user.Login, user.UserID)
		payload["logged_in"] = true

	}

	SendJSONResponse(resp, payload)
}
