package server

var htmlIndex = `
<html>

<head>
<title>OpenPLC Server</title>
<style>
* {
	font-family: Arial;
}
body {
	padding: 30px;
	background: linear-gradient(to right, #333333 0%, #444444 1%, #444444 3%, #666666 5%, #444444 50%, #333333 100%); 
}
h1 {
	background-color: #222222;
	border-radius: 9px;
	padding: 5px 10px;
	color: white;
	font-size: 20pt;
}
h1 .logo_contact {
	color: #999999;
}
h1 b {
	color:#9D0A0E;
	font-size: 24pt;
}
p a{
	color: yellow;
	padding: 10px;
	text-decoration: none;
	margin-left: 50px;
}
p a:hover {
	color: orange;
}


table {
	background-color: #efefef;
	border-radius: 4px;
	margin-left: 50px;
}
table caption {
	padding: 5px;
	color: #dddddd;
}
td, th {
	padding: 4px 10px;
	text-align: left;
	border-bottom: 1px solid dddddd;
}
tr {
	border-bottom: 1px solid dddddd;
}
table a {
	color: darkblue;
	text-decoration: none;
}
table a:hover {
	color: black;
	text-decoration: underline;
}
</style>
</head>

<body>
  <h1><span class="logo_contact">⊣</span>Open<b>PLC</b><span class="logo_contact">⊢</span> Server</h1>
  <p><a href="https://openplcproject.gitlab.io/openplc_schema/">API Docs</a></p>
  <table>
 
  <tr><th>Methods</th><th>URL</th></tr>
  {{range $index, $element := .Routes}}
	<tr>
	<td>{{range $element}}{{.}} {{end}}</td>
	<td><a href="{{$index}}">{{$index}}</a></td>
  </tr>
  {{end}}
  </ul>
</body>


</html>
`
