package server

import (
	"fmt"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
	"go.bug.st/serial.v1"
)

// DeviceType item
type DeviceType struct {
	Type     string ` json:"dev_type" `
	Label    string ` json:"label" `
	ConnType string ` json:"conn_type" `
}

// DeviceTypes lookup
var DeviceTypes = []DeviceType{
	DeviceType{Type: "Uno", Label: "Arduino Uno", ConnType: "serial"},
	DeviceType{Type: "Mega", Label: "Arduino Mega", ConnType: "serial"},
	DeviceType{Type: "ESP32", Label: "ESP32", ConnType: "net"},
	DeviceType{Type: "ESP8266", Label: "ESP8266", ConnType: "net"},
	DeviceType{Type: "TCP", Label: "Generic Modbus TCP Device", ConnType: "net"},
	DeviceType{Type: "RTU", Label: "Generic Modbus RTU Device", ConnType: "serial"},
}

// Slave Definition
type Slave struct {
	Active  bool   ` json:"active" `
	DevID   string ` json:"dev_id" validate:"required" `
	DevName string ` json:"dev_name" validate:"required" `
	DevType string ` json:"dev_type" validate:"required" `

	SlaveID int64 ` json:"slave_id" `

	ComPort  string ` json:"com_port" `
	BaudRate int64  ` json:"baud_rate" `
	Parity   string ` json:"parity" `
	DataBits int64  ` json:"data_bits" `
	StopBits int64  ` json:"stop_bits" `

	IPAddress string ` json:"ip_address" validate:"omitempty,ipv4" `
	IPPort    int64  ` json:"ip_port" validate:"required_with=IPAddress" `

	DiStart int64 ` json:"di_start" `
	DiSize  int64 ` json:"di_size" `

	CoilStart int64 ` json:"coil_start" `
	CoilSize  int64 ` json:"coil_size" `

	IrStart int64 ` json:"ir_start" `
	IrSize  int64 ` json:"ir_size" `

	HrReadStart int64 ` json:"hr_read_start" `
	HrReadSize  int64 ` json:"hr_read_size" `

	HrWriteStart int64 ` json:"hr_write_start" `
	HrWriteSize  int64 ` json:"hr_write_size" `

	ConnType    string ` json:"conn_type" `
	DateCreated string ` json:"date_created" `
	DateUpdated string ` json:"date_updated" `
	DateDeleted string ` json:"date_deleted" `
}

func (sl Slave) FilePath() string {
	return WorkSpace.SlavesDir(sl.DevID)
}

func (sl Slave) Save() error {
	sl.DateUpdated = NowStr()
	return WriteJSON(sl.FilePath(), sl)
}

func GetSlaves() ([]Slave, error) {

	slaves := make([]Slave, 0)

	files, err := ListFiles(WorkSpace.SlavesDir())
	if err != nil {
		return slaves, err
	}

	for _, dev_id := range files {
		sl, err := GetSlave(dev_id)
		if err != nil {
			fmt.Println("TODO", err)
			//return slaves, err
		} else {
			slaves = append(slaves, sl)
		}

	}
	return slaves, nil
}

func GetSlave(dev_id string) (Slave, error) {
	var sl Slave
	sl.DevID = dev_id
	err := ReadJSON(sl.FilePath(), &sl)
	return sl, err
}
func DeleteSlave(dev_id string) (bool, error) {

	return false, nil
}

// Slaves list - /ajax/slaves
func AxSlaves(resp http.ResponseWriter, req *http.Request) {

	payload := makePayload()

	payload["slaves"], payload["error"] = GetSlaves()

	SendJSONResponse(resp, payload)
}

// AxSlave requests - /ajax/slave-devices/{dev_id}
func AxSlave(resp http.ResponseWriter, req *http.Request) {

	var err error
	vars := mux.Vars(req)
	payload := makePayload()

	// get dev_id and validate
	dev_id, ok := vars["dev_id"]
	if ok && len(dev_id) < 3 {
		payload["error"] = "Missing `dev_id`"
		payload["slave"] = nil
		SendJSONResponse(resp, payload)
		return
	}

	// Delete slave
	if req.Method == "DELETE" {
		payload["deleted"], err = DeleteSlave(dev_id)
		if err != nil {
			SendJSONError(resp, err)
		}
		SendJSONResponse(resp, payload)
		return
	}

	// Update slave
	if req.Method == "POST" {
		var slUp Slave   // submitted
		var slCurr Slave // current

		err = LoadObjectFromRequestBody(&slUp, req)

		if dev_id == "new" {
			slCurr = Slave{}
			slCurr.DateCreated = NowStr()
			slCurr.DevID = GenUID()
			dev_id = slUp.DevID

		} else {
			slCurr, err = GetSlave(dev_id)
		}
		payload["pre"] = slCurr

		// This is a pain, but ain't figured out a better way
		slCurr.Active = slUp.Active
		slCurr.DevName = slUp.DevName
		slCurr.DevType = slUp.DevType
		slCurr.SlaveID = slUp.SlaveID
		slCurr.IPAddress = slUp.IPAddress
		slCurr.IPPort = slUp.IPPort
		slCurr.ComPort = slUp.ComPort
		slCurr.BaudRate = slUp.BaudRate
		slCurr.Parity = slUp.Parity
		slCurr.DataBits = slUp.DataBits
		slCurr.StopBits = slUp.StopBits

		slCurr.DiStart = slUp.DiStart
		slCurr.DiSize = slUp.DiSize
		slCurr.CoilStart = slUp.CoilStart
		slCurr.CoilSize = slUp.CoilSize
		slCurr.IrStart = slUp.IrStart
		slCurr.IrSize = slUp.IrSize
		slCurr.HrReadStart = slUp.HrReadStart
		slCurr.HrReadSize = slUp.HrReadSize
		slCurr.HrWriteStart = slUp.HrWriteStart
		slCurr.HrWriteSize = slUp.HrWriteSize

		slCurr.ConnType = slUp.ConnType
		slCurr.DateUpdated = NowStr()

		validate := validator.New()
		err := validate.Struct(slCurr)
		if err != nil {
			SendJSONError(resp, err)
			return
		}
		slCurr.Save()

	}

	// fallthough and GET
	if dev_id == "new" {
		payload["slave"] = Slave{DevID: "new"}
	} else {
		payload["slave"], payload["error"] = GetSlave(dev_id)
	}

	if true { //mode == "edit" {
		payload["device_types"] = DeviceTypes

		payload["serial_ports"], err = serial.GetPortsList()
		if err != nil {
			fmt.Println(err)
		}
	}

	SendJSONResponse(resp, payload)
}

// AxSerialPorts - lists serial ports - /ajax/serial-ports
func AxSerialPorts(resp http.ResponseWriter, req *http.Request) {

	payload := makePayload()

	payload["serial_ports"], payload["error"] = serial.GetPortsList()

	SendJSONResponse(resp, payload)
}
