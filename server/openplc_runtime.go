package server

import (
	"bufio"
	"errors"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/exec"
)

const (
	S_STOPPED   = "Stopped"
	S_RUNNING   = "Running"
	S_COMPILING = "Compiling"
	S_UNKNOWN   = "Unknown"
)

// Current runtime status
var Status string = S_UNKNOWN

// path to OpenPLC_v3/ root (new version)
var OpenPLCDir = ""

// Calls the `compile_prgram.sh` command
func ExecuteCompileProgram() error {

	cmdName := OpenPLCDir + "/scripts/compile_program.sh"
	cmdArgs := []string{"", ""}

	cmd := exec.Command(cmdName, cmdArgs...)
	cmdReader, err := cmd.StdoutPipe()
	if err != nil {
		return err
		//fmt.Fprintln(os.Stderr, "Error creating StdoutPipe for Cmd", err)
		//os.Exit(1)
	}

	scanner := bufio.NewScanner(cmdReader)
	go func() {
		for scanner.Scan() {
			fmt.Printf("compile_program out | %s\n", scanner.Text())
			SendWsMessage("compile", "console", scanner.Text())
		}
	}()

	err = cmd.Start()
	if err != nil {
		return err
		//fmt.Fprintln(os.Stderr, "Error starting Cmd", err)
		//os.Exit(1)
	}

	err = cmd.Wait()
	if err != nil {
		return err
		//fmt.Fprintln(os.Stderr, "Error waiting for Cmd", err)
		//os.Exit(1)
	}
	return nil
}

// Sends a command to the `./openplc` runtime socket
func SendCommand(cmd string) (string, error) {

	conn, err := net.Dial("tcp", "127.0.0.1:43628")
	if err != nil {
		return "", err
	}
	fmt.Fprintf(conn, cmd+"\n")

	buff := make([]byte, 1024)
	n, _ := conn.Read(buff)

	reply := string(buff[:n])
	fmt.Println(reply)
	return reply, nil
}

// Executes and starts the `openplc`
func ExecuteStartRuntime() error {

	cmdName := OpenPLCDir + "/bin/openplc"

	path, err := exec.LookPath(cmdName)
	if err != nil {
		fmt.Printf("No executable at  'cmdName' executable\n")
		return errors.New("No executable at `cmdName` executable\n")
	} else {
		fmt.Printf("'cmdName' executable is in '%s'\n", path)
	}

	cmd := exec.Command(cmdName, "")

	cmdReader, err := cmd.StdoutPipe()
	if err != nil {
		SendWsMessage("runtime", "error", "Error creating StdoutPipe for Cmd")
		//fmt.Fprintln(os.Stderr, "Error creating StdoutPipe for Cmd", err)
		return err
	}

	scanner := bufio.NewScanner(cmdReader)
	go func() {
		for scanner.Scan() {
			ll := scanner.Text()
			//fmt.Printf("\t > %s\n", ll)
			fmt.Println("=", ll)

			SendWsMessage("runtime", "console", ll)
		}
	}()

	err = cmd.Start()
	if err != nil {
		SendWsMessage("runtime", "error", "Error starting Cmd")
		fmt.Fprintln(os.Stderr, "Error starting Cmd", err)
		return err
	} else {
		SendWsMessage("runtime", "console", "Started")
	}

	err = cmd.Wait()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error waiting for Cmd", err)
		return err
	}
	return nil
	// // https://github.com/gorilla/websocket/blob/master/examples/command/main.go
	// outr, outw, err := os.Pipe()
	// if err != nil {
	// 	wsInternalError2("stdout:", err)
	// 	return err
	// }
	// defer outr.Close()
	// defer outw.Close()

	// inr, inw, err := os.Pipe()
	// if err != nil {
	// 	wsInternalError2("stdin:", err)
	// 	return err
	// }
	// defer inr.Close()
	// defer inw.Close()

	// fmt.Println("starting", cmdName)
	// proc, err := os.StartProcess(cmdName, cmdArgs, &os.ProcAttr{
	// 	Files: []*os.File{inr, outw, outw},
	// })
	// if err != nil {
	// 	wsInternalError2("start:", err)
	// 	return err
	// }

	// inr.Close()
	// outw.Close()

	// stdoutDone := make(chan struct{})
	// go pumpStdout(outr, stdoutDone)
	// //go ping(ws, stdoutDone)

	// //pumpStdin(ws, inw)

	// // Some commands will exit when stdin is closed.
	// inw.Close()

	// // Other commands need a bonk on the head.
	// if err := proc.Signal(os.Interrupt); err != nil {
	// 	log.Println("inter:", err)
	// 	fmt.Println("inter:", err)
	// }

	// select {
	// case <-stdoutDone:
	// case <-time.After(time.Second):
	// 	// A bigger bonk on the head.
	// 	if err := proc.Signal(os.Kill); err != nil {
	// 		log.Println("term:", err)
	// 		fmt.Println("term:", err)
	// 	}
	// 	<-stdoutDone
	// }

	// if _, err := proc.Wait(); err != nil {
	// 	log.Println("wait:", err)
	// 	fmt.Println("wait:", err)
	// }
	// return nil
	/*
		fmt.Println("OPENPLC ExecuteStartRuntime()", cmdName, OpenPLC_DIR+"/build")

		cmd := exec.Command(cmdName, cmdArgs...)
		cmd.Dir = OpenPLC_DIR + "/build"

		cmdReader, err := cmd.StdoutPipe()
		if err != nil {
			fmt.Println("OPENPLC reader error", err)
			return err
		}

		scanner := bufio.NewScanner(cmdReader)
		go func() {
			for scanner.Scan() {
				fmt.Println("OPENPLC console", err)
				SendWsMessage("runtime", "console", scanner.Text())
			}
		}()

		err = cmd.Start()
		if err != nil {
			fmt.Println("OPENPLC start()", err)
			return err
		}
		SendWsMessage("runtime", "console", "Started command")
		fmt.Println("OPENPLC started", err)
		err = cmd.Wait()
		if err != nil {
			fmt.Println("OPENPLC wait()", err)
			return err
		}
		return nil
	*/
}

// Stops the `openplc` runtime
func StopRuntime() (string, error) {
	return SendCommand("quit()")
}

// Starts the openplc runtime - /ajax/runtime/start
func AxRuntimeStart(resp http.ResponseWriter, req *http.Request) {

	payload := makePayload()

	go ExecuteStartRuntime()

	SendJSONResponse(resp, payload)
}

// Stops the openplc runtime - /ajax/runtime/stop
func AxRuntimeStop(resp http.ResponseWriter, req *http.Request) {

	payload := makePayload()

	reply, err := StopRuntime()
	if err != nil {
		payload["error"] = err.Error()
	}
	payload["reply"] = reply

	SendJSONResponse(resp, payload)
}
