package server

import (
	"html/template"
	"net/http"
)

// Home Page html
func P_Home(resp http.ResponseWriter, req *http.Request) {

	data := make(map[string]interface{})
	data["Routes"] = GetRoutesInfo()

	t := template.Must(template.New("tmpl").Parse(htmlIndex))

	t.ExecuteTemplate(resp, "tmpl", data)

}
