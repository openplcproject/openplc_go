openplc_go
==================

New web server for openplc written in go

## Features

- xplatform binaries (with embedded assets)
- REST api
- websockets for monitoring and shell output
- multithreaded non blocking
- embeded AJAX GUI  (vuejs)
- fast, lean & mean

## Download & Run

- The easiset way is to download a build from 
  [downloads/](https://openplcproject.gitlab.io/openplc_go/md_build_index.html)

```bash
# download eg for linux-amd64
wget https://openplcproject.gitlab.io/openplc_go/downloads/openplc-server-linux-amd64

# make it executable
chmod +x ./openplc_server-linux-amd64

# Run the server
# -w path to workspace where stuff is stored (required)
# -o path to OpenPLC_v3/ git checkout (required)
# -a address, defaults to 127.0.0.1:8080
# -d enable debug
./openplc_server-linux-amd64 -w ~/my_openplc_stuff -o ~/OpenPLC_v3 

# Hit a browser at address -a config
http://localhost:8080
```

**NOTE:**
 - the `-o` flag expects `/scripts/compile_program.sh` and `/build/openplc`


## URL's & End Points

- visit https://openplcproject.gitlab.io/openplc_schema/


## ApiDocs

 - [godocs](https://godoc.org/gitlab.com/openplcproject/openplc_go)



## Development

NOTE: golang 1.11+ is required with the new "modules"

### Structure
- `/cmd/openplc_server/` - main command
- `/server` - http, ajax, websocket server

```
git clone gitlab.com/openplcproject/openplc_go

cd openplc_go
go mod download

go run cmd/openplc_server.main.go -w /dir/myspace -o /path/to/OpenPLC_v3
```

#### R+D
- directly connecting to modbus, dnp3
- glob


